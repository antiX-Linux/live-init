��          \      �       �      �   *   �      �   
          �  3  0   �    �  	     -        C     `  2   l  �  �  6   Y                                       Choose Choose ONE repository for Debian and antiX No item selected Repository The installer will now run. This tool sets your default Debian repository and then runs the installer.

You can choose between:

Jessie/Stable
Stretch/New Stable (Experts)
Testing (Experts)
Sid/Unstable (Experts)

This release of antiX defaults to Jessie/Stable and antiX/Stable.

If you do not know what to do, choose Cancel and the 
Jessie/Stable repositories will be used and the installer will begin.

Press OK to continue. Your Debian and antiX repositories are set to $x Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 00:12+0300
PO-Revision-Date: 2019-01-25 17:37+0200
Last-Translator: Ivica  Kolić <ikoli@yahoo.com>
Language-Team: Croatian (http://www.transifex.com/anticapitalista/antix-development/language/hr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hr
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Poedit 1.8.11
 Odaberite Odaberite JEDAN repozitorij za Debian i antiX NIjedna stavka nije izabrana Repozitorij Instalacijski program će sada započeti sa radom. Ovaj alat postavlja vaš zadani Debian repozitorij i zatim pokreće instalacijski program.

Možete birati između:

Jessie/Stable
Stretch/New Stable (Experts)
Testing (Experts)
Sid/Unstable (Experts)

Ovo antiX idanje ima kao zadano Jessie/Stable and antiX/Stable.

Ako ne znate što učiniti, izaberite Otkaži i 
Jessie/Stable repozitoriji će biti korišteni i instalacijski program će započeti s radom..

Pritisnite Uredu za nastavak. Vaši Debian i antiX repozitoriji su postavljeni na $x 
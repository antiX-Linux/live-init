��          \      �       �      �   *   �      �   
          �  3  0   �  �  �     �  $   �     �            �  -  2   �                                       Choose Choose ONE repository for Debian and antiX No item selected Repository The installer will now run. This tool sets your default Debian repository and then runs the installer.

You can choose between:

Jessie/Stable
Stretch/New Stable (Experts)
Testing (Experts)
Sid/Unstable (Experts)

This release of antiX defaults to Jessie/Stable and antiX/Stable.

If you do not know what to do, choose Cancel and the 
Jessie/Stable repositories will be used and the installer will begin.

Press OK to continue. Your Debian and antiX repositories are set to $x Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 00:12+0300
PO-Revision-Date: 2019-01-25 17:39+0200
Last-Translator: mahmut özcan <mahmutozcan65@yahoo.com>
Language-Team: Turkish (http://www.transifex.com/anticapitalista/antix-development/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
 Seç Debian ve antiX için BİR depo seç Seçilmiş öğe yok Depo Yükleyici şimdi çalışacak. Bu araç, öntanımlı Debian deponuzu ayarlar ve yükleyiciyi çalıştırır.

Şunlardan birini seçebilirsiniz:

Jessie/Kararlı
Stretch/Yeni Kararlı (Uzmanlar)
Deneme (Uzmanlar)
Sid/Kararsız (Uzmanlar)

Bu antiX dağıtımı öntanımlı olarak Jessie/Kararlı antix/Kararlı'dır

Ne yapacağınızı bilmiyorsanız İptali seçin ve
Jessie/Kararlı depolar kullanılacak ve kurulrm başlayacak.
Devam etmek için TAMAMa basın. Debian ve antiX depolarınız $x olarak ayarlandı 
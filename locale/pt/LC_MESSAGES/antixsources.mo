��          \      �       �      �   *   �      �   
          �  3  0   �  �  �     �  +   �     �       0   !  �  R  8   M                                       Choose Choose ONE repository for Debian and antiX No item selected Repository The installer will now run. This tool sets your default Debian repository and then runs the installer.

You can choose between:

Jessie/Stable
Stretch/New Stable (Experts)
Testing (Experts)
Sid/Unstable (Experts)

This release of antiX defaults to Jessie/Stable and antiX/Stable.

If you do not know what to do, choose Cancel and the 
Jessie/Stable repositories will be used and the installer will begin.

Press OK to continue. Your Debian and antiX repositories are set to $x Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 00:12+0300
PO-Revision-Date: 2019-01-25 17:38+0200
Last-Translator: José Vieira <jvieira33@sapo.pt>
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Escolher Escolha UM repositório para Debian e antiX Nenhum item seleccionado Repositório O programa de instalação será agora executado Esta ferramenta estabelece o repositório Debian pré-definido e de seguida executa o instalador.

Pode escolher entre:

Jessie/Stable
Stretch/New Stable (Utilizadores experientes)
Testing (Utilizadores experientes)
Sid/Unstable (Utilizadores experientes)

Esta versão do antiX escolhe automáticamente os repositórios Jessie/Stable e antiX/Stable. 

Se não souber o que fazer, prima Cancelar.
Serão seleccionados os repositórios Jessie/Stable e o instalador será executado.

Prima OK para continuar. Os repositórios Debian e antiX estão definidos para $x 
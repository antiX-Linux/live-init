��          \      �       �      �   *   �      �   
          �  3  0   �  �  �  
   �  5   �     )     F  $   O  �  t  5   Q                                       Choose Choose ONE repository for Debian and antiX No item selected Repository The installer will now run. This tool sets your default Debian repository and then runs the installer.

You can choose between:

Jessie/Stable
Stretch/New Stable (Experts)
Testing (Experts)
Sid/Unstable (Experts)

This release of antiX defaults to Jessie/Stable and antiX/Stable.

If you do not know what to do, choose Cancel and the 
Jessie/Stable repositories will be used and the installer will begin.

Press OK to continue. Your Debian and antiX repositories are set to $x Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 00:12+0300
PO-Revision-Date: 2019-01-25 17:37+0200
Last-Translator: Moo
Language-Team: Lithuanian (http://www.transifex.com/anticapitalista/antix-development/language/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.11
 Pasirinkti Pasirinkite VIENĄ saugyklą, skirtą Debian ir antiX Nepasirinktas joks elementas Saugykla Dabar bus paleista diegimo programa. Šis įrankis nustato jūsų numatytąją Debian saugyklą, o tuomet paleidžia diegimo programą.

Jūs galite rinktis iš šių:

Jessie/Stable
Stretch/New Stable (Ekspertai)
Testing (Ekspertai)
Sid/Unstable (Ekspertai)

Šios antiX laidos numatytosios saugyklos yra Jessie/Stable ir antiX/Stable.

Jeigu nežinote ką daryti, pasirinkite Atsisakyti ir tuomet bus naudojamos 
Jessie/Stable saugyklos, ir diegimo programa pradės darbą.

Norėdami tęsti, paspauskite Gerai. Jūsų Debian ir antiX saugyklos yra nustatytos į $x 
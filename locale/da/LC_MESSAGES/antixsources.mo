��          \      �       �      �   *   �      �   
          �  3  0   �  �  �     �  +   �     �     �  "     �  0  9   �                                       Choose Choose ONE repository for Debian and antiX No item selected Repository The installer will now run. This tool sets your default Debian repository and then runs the installer.

You can choose between:

Jessie/Stable
Stretch/New Stable (Experts)
Testing (Experts)
Sid/Unstable (Experts)

This release of antiX defaults to Jessie/Stable and antiX/Stable.

If you do not know what to do, choose Cancel and the 
Jessie/Stable repositories will be used and the installer will begin.

Press OK to continue. Your Debian and antiX repositories are set to $x Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 00:12+0300
PO-Revision-Date: 2019-01-25 17:40+0200
Last-Translator: scootergrisen
Language-Team: Danish (http://www.transifex.com/anticapitalista/antix-development/language/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Vælg Vælg ÉN softwarekilde til Debian og antiX Der er ikke valgt noget punkt Softwarekilde Installationsprogrammet køres nu. Værktøjet indstiller din standard-Debian-softwarekilde og kører herefter installationsprogrammet.

Du kan vælge mellem:

Jessie/Stable
Stretch/New Stable (eksperter)
Testing (eksperter)
Sid/Unstable (eksperter)

Denne udgivelse af antiX bruger som standard Jessie/Stable og antiX/Stable.

Hvis du ikke ved hvad du skal gøre, så vælg Annuller, så bruges 
Jessie/Stable-softwarekilderne og installationsprogrammet startes.

Tryk på OK for at fortsætte. Dine Debian- og antiX-softwarekilder er indstillet til $x 
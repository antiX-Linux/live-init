��          \      �       �      �   *   �      �   
          �  3  0   �  �  �     �  +   �       
   -     8  �  W  +                                          Choose Choose ONE repository for Debian and antiX No item selected Repository The installer will now run. This tool sets your default Debian repository and then runs the installer.

You can choose between:

Jessie/Stable
Stretch/New Stable (Experts)
Testing (Experts)
Sid/Unstable (Experts)

This release of antiX defaults to Jessie/Stable and antiX/Stable.

If you do not know what to do, choose Cancel and the 
Jessie/Stable repositories will be used and the installer will begin.

Press OK to continue. Your Debian and antiX repositories are set to $x Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 00:12+0300
PO-Revision-Date: 2019-01-25 17:38+0200
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Slovak (http://www.transifex.com/anticapitalista/antix-development/language/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Poedit 1.8.11
 Vybrať Vyberte JEDEN repozitár pre Debian a antiX Nič nevybrané Repozitár Inštalátor sa teraz spustí. Tento nástroj nastavuje repozitáre Debian-u a potom spúšťa inštalátor.

Môžete si vybrať z:

Jessie/Stable
Stretch/New Stable (iba pre expertov)
Testovací (iba pre expertov)
Sid/Nestabilný (iba pre expertov)

Toto vydanie antiX používa repozitáre Jessie/Stable a antiX/Stable.

Ak si neviete rady, zvoľte Zatvoriť pre použitie 
repozitára Stretch/Stabilný, potom sa spustí inštalátor.

Stlačte OK pre pokračovanie. Vaše repozitáre Debian-u a antiX-u sú $x 
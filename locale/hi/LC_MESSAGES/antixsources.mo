��          \      �       �      �   *   �      �   
          �  3  0   �  �  �     �  [   �  -   /  "   ]  <   �  �  �  [   �	                                       Choose Choose ONE repository for Debian and antiX No item selected Repository The installer will now run. This tool sets your default Debian repository and then runs the installer.

You can choose between:

Jessie/Stable
Stretch/New Stable (Experts)
Testing (Experts)
Sid/Unstable (Experts)

This release of antiX defaults to Jessie/Stable and antiX/Stable.

If you do not know what to do, choose Cancel and the 
Jessie/Stable repositories will be used and the installer will begin.

Press OK to continue. Your Debian and antiX repositories are set to $x Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 00:12+0300
PO-Revision-Date: 2019-01-25 17:36+0200
Last-Translator: Panwar108 <caspian7pena@gmail.com>
Language-Team: Hindi (http://www.transifex.com/anticapitalista/antix-development/language/hi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 चुनें Debian व antiX के लिए एक पैकेज-संग्रह चुनें कुछ चयनित नहीं है पैकेज-संग्रह अब इंस्टॉलर चालू होगा। यह साधन आपका डिफ़ॉल्ट Debian पैकेज-संग्रह सेट कर, इंस्टॉलर चालू करता है।

आप इनमें से चुन सकते हैं :

Jessie/स्थिर
Stretch/नवीनतम स्थिर (निपुण उपयोक्ता)
Testing (निपुण उपयोक्ता)
Sid/अस्थिर (निपुण उपयोक्ता)

इस antiX संस्करण का डिफ़ॉल्ट Jessie/स्थिर व antiX/स्थिर है।

अगर आपको नहीं पता कि क्या चुनना है, तो रद्द करें चुनें और 
Jessie/स्थिर पैकेज-संग्रह उपयोग किया जाएगा व इंस्टॉलर चालू होगा।

जारी रखने के लिए ठीक है दबाएँ। आपके Debian व antiX पैकेज-संग्रह $x पर सेट है 
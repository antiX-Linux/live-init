��          \      �       �      �   *   �      �   
          �  3  0   �  �  �     �  3   �     	          .  �  N  @   /                                       Choose Choose ONE repository for Debian and antiX No item selected Repository The installer will now run. This tool sets your default Debian repository and then runs the installer.

You can choose between:

Jessie/Stable
Stretch/New Stable (Experts)
Testing (Experts)
Sid/Unstable (Experts)

This release of antiX defaults to Jessie/Stable and antiX/Stable.

If you do not know what to do, choose Cancel and the 
Jessie/Stable repositories will be used and the installer will begin.

Press OK to continue. Your Debian and antiX repositories are set to $x Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 00:12+0300
PO-Revision-Date: 2019-01-25 17:35+0200
Last-Translator: En Kerro <inactive+ekeimaja@transifex.com>
Language-Team: Finnish (http://www.transifex.com/anticapitalista/antix-development/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Valitse Valitse YKSI pakettivarasto Debianille ja antiX:lle Ei mitään valittuna Pakettivarasto Asennusohjelma käynnistyy nyt. Tämä työkalu tekee Debian-pakettivaraston oletusasetukset ja käynnistää sitten asennusohjelman.

Voit valita seuraavista vaihtoehdoista:

Jessie/Vakaa
Stretch/New Stable (Kokeneille)
Testing (Kokeneille)
Sid/Epävakaa (Kokeneille)

Tämä antiX-julkaisu käyttää oletuksena Jessie/Stable sekä antiX/Stable -pakettivarastoja.

Jos et tiedä mitä tehdä, paina Peru 
jolloin käytetään Jessie/Vakaa -pakettivarastoja ja asennusohjelma käynnistyy.

Paina OK jatkaaksesi. Sinun Debianin ja antiX:in pakettivarastoiksi on määritetty $x 